// Gmsh project created on Thu Dec 20 22:08:34 2018
SetFactory("OpenCASCADE");
//+
Box(1) = {-25, -25, -35, 50, 50, 70};
//+
Sphere(2) = {0, 0, 0, 5, -Pi/2, Pi/2, 2*Pi};
//+
Sphere(3) = {0, 0, 0, 5, -Pi/2, Pi/2, 2*Pi};
//+
BooleanDifference{ Volume{1}; Delete; }{ Volume{2}; }
//+
Recursive Delete {
  Volume{3}; 
}
//+
Physical Volume("charge", 1) = {2};
//+
Physical Volume("background", 2) = {1};
//+
Physical Surface("top", 3) = {11};
//+
Physical Surface("bottom", 4) = {13};
//+
Physical Surface("sides", 5) = {10, 9, 12, 14};
//+
Physical Surface("interface", 6) = {7};
//+
Transfinite Line {14} = 30 Using Progression 1;
//+
Transfinite Line {19, 21, 29, 24} = 15 Using Progression 1;
//+
Transfinite Line {25, 20, 26, 27, 28, 30, 23, 22} = 10 Using Progression 1;
