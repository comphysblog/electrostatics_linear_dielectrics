// Gmsh project created on Tue Jan  8 20:36:04 2019
SetFactory("OpenCASCADE");
//+
Disk(1) = {0, 0, 0, 3, 3};
//+
Rectangle(2) = {-40, -25, 0, 80, 50, 0};
//+
BooleanDifference{ Surface{2}; Delete; }{ Surface{1}; }
//+
Physical Surface("dielectric", 1) = {1};
//+
Physical Surface("vacuum", 2) = {2};
//+
Physical Line("top", 3) = {5};
//+
Physical Line("bottom", 4) = {2};
//+
Physical Line("sides", 6) = {3, 4};
//+
Physical Line("interface", 7) = {1};
//+
Transfinite Line {1} = 60 Using Progression 1;
//+
Transfinite Line {3, 5, 4, 2, 2} = 30 Using Progression 1;
