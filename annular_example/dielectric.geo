// Gmsh project created on Tue Dec 11 00:04:48 2018
SetFactory("OpenCASCADE");
//+
Disk(1) = {0, 0, 0, 5, 5};
//+
Disk(2) = {0, 0, 0, 4, 4};
//+
Disk(3) = {0, 0, 0, 2, 2};
//+
Disk(4) = {0, 0, 0, 0.2, 0.2};
//+
BooleanDifference{ Surface{2}; Delete; }{ Surface{3}; Delete; }
//+
BooleanDifference{ Surface{1}; Delete; }{ Surface{2}; }
//+
BooleanDifference{ Surface{6}; Delete; }{ Surface{4}; }
//+
Physical Surface("charge", 1) = {4};
//+
Physical Surface("dielectric", 2) = {2};
//+
Physical Surface("background_inner", 3) = {5};
//+
Physical Surface("background_outer", 4) = {6};
//++
Physical Line("line_boundary", 5) = {4};
//+
Physical Line("dielectric_inner", 6) = {3};
//+
Physical Line("dielectric_outer", 7) = {5};
//+
Physical Line("outer", 8) = {6};
